package analizadorlexicojflex;
%%
%class Lexer
%type String
%line
%{
    public int getLine(){
        return yyline;
    }
%}
%{
    public String lexeme;
%}
L = [a-zA-Z]
D = [0-9]
N = \-[0-9]+
S = \"([^\"]*)\"
R = -?[0-9]+(,[0-9]+)

WHITE = [ \t\r\n]
%%
{WHITE} {/*Ignore*/}
"if" {return " IF";}
"else" {return " ELSE";}
"switch" {return " SWITCH";}
"case" {return " CASE";}
"=" {return " ASIGNACION";}
">" {return " MAYOR_QUE";}
">=" {return " MAYOR_IGUAL";}
"+" {return " SUMA";}
"-" {return " RESTA";}
"&&" {return " AND";}
"==" {return " IGUAL";}
";" {return " PUNTO_Y_COMA";}
"(" {return " PAR_ABRE";}
")" {return " PAR CIERRA";}

"//".* {return " COMENTARIOS_UNA_LINEA";}
"||" {return " OR";}
"!" {return " NEGACION";}
"!=" {return " DIFERENTE";}
"*" {return " MULTIPLICACION";}
"/" {return " DIVISION";}
"%" {return " RESIDUO";}
"true" {return " TRUE";}
"false" {return " FALSE";}

"{" {return " LLAVE_ABRE";}
"}" {return " LLAVE_CIERRA";}

{L}({L}|{D})* {lexeme = yytext(); return "ID";}
{D}+ {lexeme = yytext(); return "INT";}

{N}+ {lexeme = yytext(); return "NUM_NEGATIVO";}
{S}+ {lexeme = yytext(); return "STRING_DOBLE_COMILLA";}
{R}+ {lexeme = yytext(); return "NUM_REAL";}
. {return "ERROR";}
